make clean
make all
#make stencil_serial
#make stencil_tbb
echo "serial"
time ./stencil_serial image.jpg
echo "openmp"
time ./stencil_mp image.jpg
echo "cilk"
time ./stencil_cilk image.jpg
echo "tbb"
time ./stencil_tbb image.jpg
echo "re serial"
time ./stencil_re image.jpg

