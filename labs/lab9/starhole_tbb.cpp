#define _USE_MATH_DEFINES
#include <cmath>
#include <cstdint>
#include <stdlib.h>
#include <stdio.h>
#include <opencv2/opencv.hpp>
#include <tbb/tbb.h>
#include <tbb/task.h>
#include <pthread.h>
using namespace cv;

#include "starhole_common.cpp"

// Represents the area in which the particles move
static DirUpdate*   area;

// Holds the split probabilities for Carter particles in cells
static double*      splitProb;

// Holds final counts of particles in cells
static int* outArea;

// Configuration parameters
static int radius;
static int sim_steps;

// Returns the total number of particles descending from this call
// and increments the count at the right location
class walker: public tbb::task
{
public: 
    struct drand48_data seedbuf;
    long int seed;
    int x,y;
    int stepsremaining;
    int * particles;
    walker(long int s,int x,int y,int steps,int * p):seed(s),x(x),y(y),stepsremaining(steps),particles(p)
    {
        //*particles = 1;
        //srand48_r(seed,&seedbuf);
    };

    task* execute()
    {
        *particles = 1;
        srand48_r(seed,&seedbuf);
        //tbb::task_list tkl;
        //int tasks = 1;
        int step = stepsremaining;
        int aka[stepsremaining];
        set_ref_count(1);
        for( ; stepsremaining>0 ; stepsremaining-- ) {
            aka[stepsremaining - 1] = 0;
            //Does the Carter particle split? If so, start the walk for the new one
            if(doesSplit(&seedbuf, splitProb, x, y, radius)) {
                //printf("spliting!\n");
                long int newseed;
                lrand48_r(&seedbuf, &newseed);
                walker& tk = *new( allocate_child() )  walker(seed + newseed, x, y, stepsremaining-1,&aka[stepsremaining-1]);
                //tkl.push_back(tk);
                //set_ref_count(++tasks);
                spawn(tk);
                increment_ref_count();
            }
        
        // Make the particle walk?
            updateLocation(&seedbuf, area, &x, &y, radius);
        }
        
        //if(tasks > 1)
        //{
            //printf("tasks = %d\n",tasks);
            //set_ref_count(tasks);
            //printf("spawning");
        wait_for_all();
            //printf("spawn complete\n");
        //}
        //printf("%d ",tasks);
        //printf("summing\n");
        for(int i = 0; i <step; i++) *particles += aka[i];
        //delete(aka);
        // record the final location
        outArea[toOffset(x,y,radius)] += 1;
        //printf("summing complete\n");
        //printf("%d",*particles);
        return NULL;
    }
};





/*
int walker(long int seed, int x, int y, int stepsremaining) {
    struct drand48_data seedbuf;
    srand48_r(seed, &seedbuf);
    int particles = 1;
    for( ; stepsremaining>0 ; stepsremaining-- ) {
        
        // Does the Carter particle split? If so, start the walk for the new one
        if(doesSplit(&seedbuf, splitProb, x, y, radius)) {
            //printf("spliting!\n");
            long int newseed;
            lrand48_r(&seedbuf, &newseed);
            particles += walker(seed + newseed, x, y, stepsremaining-1);
        }
        
        // Make the particle walk?
        updateLocation(&seedbuf, area, &x, &y, radius);
    }
    
    // record the final location
    outArea[toOffset(x,y,radius)] += 1;
    
    return particles;
}*/

int main(int argc, char** argv) {
    if(argc<6 || ((argc-4)%2 != 0)) {
        printf("Usage: %s <steps> <radius> <amount> <x1> <y1> ... <xN> <yN>\n",argv[0]);
        return 1;
    }

    printf("Attempting to setup initial state...\n");
    // Initialize simulation Params
    int* coords;
    int coordPairs, amount;
    readArgs(argc, argv, &sim_steps, &radius, &amount, &coordPairs, &coords);
    
    // Initialize simulation lookups
    initialize(radius,&outArea,&splitProb,&area);
    
    
    // Start initial walks
    printf("Starting the walks...\n");
    int totParticles = 0;
    for(int i=0;i<coordPairs*2;i+=2) {
        for(int j=0;j<amount;j++) {
            int initialparticle;// = 1;
            walker& tk = *new(tbb::task::allocate_root() )  walker(i+j, coords[i], coords[i+1], sim_steps,&initialparticle);
	    //printf("spawn root %d<%d*%d<%d = %d\n",i,coordPairs,j,amount,totParticles);
            tbb::task::spawn_root_and_wait(tk);
            totParticles += initialparticle;//walker(i+j, coords[i], coords[i+1], sim_steps);
        }
    }
    printf("Walks complete... finished with %d particles\n",totParticles);
   
    // Generate the output
    writeOutput(radius, outArea);

    free(coords);
    free(outArea);
    free(splitProb);
    free(area);
}
