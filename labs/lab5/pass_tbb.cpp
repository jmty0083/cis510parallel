#include <tbb/tbb.h>
#include <pthread.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <math.h>
#include <openssl/md5.h>

using namespace tbb;

const char* chars="0123456789";

// tests if a hash matches a candidate password
int test(const char* passhash, const char* passcandidate) {
    unsigned char digest[MD5_DIGEST_LENGTH];
    
    MD5((unsigned char*)passcandidate, strlen(passcandidate), digest);
    
    char mdString[34];
    mdString[33]='\0';
    for(int i=0; i<16; i++) {
        sprintf(&mdString[i*2], "%02x", (unsigned char)digest[i]);
    }
    return strncmp(passhash, mdString, strlen(passhash));
}

// maps a PIN to a string
void genpass(long passnum, char* passbuff) {
    passbuff[8]='\0';
    int charidx;
    int symcount=strlen(chars);
    //#pragma omp parallel for
    /*for(int i=7; i>=0; i--) {
        int thisNumber = passnum/pow(symcount,i);
	charidx=thisNumber%symcount;
        //passnum=passnum/symcount;
        passbuff[i]=chars[charidx];
    }*/
    for(int i=7; i>=0; i--) {
        charidx=passnum%symcount;
        passnum=passnum/symcount;
        passbuff[i]=chars[charidx];
    }
}

class Found
{
public:
    Found(char * o,char * a,int * result)
    {
        passhash = o;
        passmatch = a;
        notfound = result;
    };
    int * notfound;
    char * passhash;
    char * passmatch;
    void operator()(const blocked_range<int> & range) const
    {
        for(int i=range.begin();i!=range.end();i++)
        {
             if(*notfound ==0) continue;
             genpass(i,passmatch);
             //printf("%s - %s :%d\n",passhash,passmatch,i);
             *notfound = test(passhash, passmatch);//) break;
             //printf("%s - %s\n",passhash,passmatch);
        }
    }
};


//void paraFound(char * passhash,char * passmatch)
//{
//    parallel_for(blocked_range<int>(0,1000000000),Found newFound(passmatch));
//}

int main(int argc, char** argv) {
    if(argc != 2) {
        printf("Usage: %s <password hash>\n",argv[0]);
        return 1;
    }
    char passmatch[9];
    //long currpass=0;
    int notfound=1;
    //#pragma omp parallel for
    /*for(currpass;currpass< 1000000000;currpass++)
    {
        genpass(currpass,passmatch);
        //printf("%d\n",currpass);
        notfound=test(argv[1], passmatch);
        //currpass++;
    }*/
    parallel_for(blocked_range<int>(0,100000000),Found(argv[1],passmatch,&notfound));
    //paraFound(argv[1],passmatch);
    printf("found: %s\n",passmatch);
    return 0;
}


